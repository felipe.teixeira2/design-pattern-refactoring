class PurchasesController < ApplicationController

  VALID_GATEWAYS = ['paypal', 'stripe']

  def valid_gateway?
    gateway = purchase_params[:gateway]
    VALID_GATEWAYS.include? gateway
  end

  def get_cart
    cart_id = purchase_params[:cart_id]
    @cart = Cart.find_by(id: cart_id)
  end

  def valid_cart?
    get_cart
  end

  def create_guest_user
    user_params = purchase_params[:user] || {}
    User.create(**user_params.merge(guest: true))
  end

  def get_user
    @user = @cart.user if @cart
    @user = create_guest_user unless @user
  end

  def valid_user?
    get_user
    @user.valid?
  end

  def compose_order
    new_order
    @order.save
    @order.items = expand_items(@cart.items)
  end

  def new_order
    @order = Order.new(
      user: @user,
      first_name: @user.first_name,
      last_name: @user.last_name,
      address_1: address_params[:address_1],
      address_2: address_params[:address_2],
      city: address_params[:city],
      state: address_params[:state],
      country: address_params[:country],
      zip: address_params[:zip]
    )
  end

  def expand_items(items)
    items.flat_map do |item|
      item.quantity.times.map do
        new_item(item)
      end
    end
  end

  def new_item(item)
    OrderLineItem.new(
      order: @order,
      sale: item.sale,
      unit_price_cents: item.sale.unit_price_cents,
      shipping_costs_cents: shipping_costs,
      paid_price_cents: item.sale.unit_price_cents + shipping_costs
    )
  end

  def create
    return render json: { errors: [{ message: 'Gateway not supported!' }] },
      status: :unprocessable_entity unless valid_gateway?

    return render json: { errors: [{ message: 'Cart not found!' }] },
      status: :unprocessable_entity unless valid_cart?

    return render json: { errors: @user.errors.map(&:full_message).map { |message| { message: message } } },
      status: :unprocessable_entity unless valid_user?

    compose_order

    render json: { status: :success, order: { id: @order.id } }, status: :ok
  end

  private

  def purchase_params
    params.permit(
      :gateway,
      :cart_id,
      user: %i[email first_name last_name],
      address: %i[address_1 address_2 city state country zip]
    )
  end

  def address_params
    purchase_params[:address] || {}
  end

  def shipping_costs
    100
  end
end
